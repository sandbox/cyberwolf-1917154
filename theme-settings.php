<?php

/**
 * @file
 * Hook implementations for the Paddle Modern theme.
 */

/**
 * Implements hook_paddle_themer_style_set().
 */
function paddle_modern_theme_paddle_themer_style_set() {
  // Define default styles to apply to each selector.
  $styles = paddle_modern_theme_styles(array(
    'color' => array(),
    'font' => array(
      'settings' => array(
        'font families' => array(
          'Arial',
          'Helvetica',
          'Times New Roman',
        ),
      ),
    ),
    'padding' => array(),
  ));

  // Map selectors to titles.
  $selector_titles = array(
    'h1' => t('H1 Heading'),
    'h2' => t('H2 Heading'),
    'h3' => t('H3 Heading'),
    'p' => t('Paragraph'),
    'blockquote' => t('Quote'),
    'ul, ul li, ol, ol li' => t('List'),
    'a' => t('Link'),
  );

  // Build the list of selectors.
  $selectors = array();
  foreach ($selector_titles as $selector => $title) {
    $selectors[$selector] = array(
      'title' => $title,
      'styles' => $styles,
    );
  }
  // Return the selector list as the required 'global' style set.
  return array(
    'global' => array(
      'title' => t('Global styles'),
      'selectors' => $selectors,
    ),
  );
}

/**
 * Generates a list of styles for hook_paddle_themer_style_set().
 *
 * @see hook_paddle_themer_style_set()
 *
 * @param array $plugins
 *   An array of style plugins that will be used to generate the style list. The
 *   style plugins may be given as either a string or an array:
 *   - string: the style plugin will be listed with default options.
 *   - array keyed by style plugin: pass custom options in the following keys:
 *     - module (optional): the module that provides the style. If omitted the
 *       value from the $module parameter will be used.
 *     - settings (optional): an optional array of settings to pass to the style
 *       plugin.
 * @param string $module
 *   The module that will be used by default as a styles plugin provider if no
 *   other module is specified.
 *
 * @return array
 *   An array of style options as required by hook_paddle_themer_style_set().
 */
function paddle_modern_theme_styles($plugins, $module = 'paddle_style') {
  $styles = array();
  foreach ($plugins as $key => $plugin) {
    $styles[] = array(
      'module' => is_array($plugin) && !empty($plugin['module']) ? $plugin['module'] : $module,
      'plugin' => is_array($plugin) ? $key : $plugin,
      'settings' => is_array($plugin) && !empty($plugin['settings']) ? $plugin['settings'] : array(),
    );
  }
  return $styles;
}
